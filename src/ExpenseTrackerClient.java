import com.fasterxml.jackson.databind.ObjectMapper;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

public class ExpenseTrackerClient {

    private static String serverURL = "http://localhost:8082/FaderinExpenseTracker/";
    private final static String currency = "NGN";
    private static ObjectMapper mapper = new ObjectMapper();

    private static Scanner scanner = new Scanner(System.in).useDelimiter("\\n");
    static String[] menuItems = {"Create Authenticated User", "Enter Expense",
            "View All Users", "View Expense by user", "Close Application"};
    boolean programIsRunning = true;
    private static String applicationName = "=======Expense======";

    public static void main(String[] args) {
        //Add servlet path to server url
        serverURL = serverURL + "ExpenseServlet";
        displayMenu();
    }

    private static void displayMenu() {
        ///first clear the screen
        clearConsole();
        //////show application name and menu below
        System.out.println(applicationName + "\n");
        System.out.println("Please select an option below to run programme.");
        for (int i = 0; i < menuItems.length; i++) {
            System.out.println(i + 1 + ". " + menuItems[i]);
        }
        System.out.println("");
        processMenu();
    }

    private static void processMenu() {
        /////receive menu input from user
        String input = scanner.next();
        switch (input) {
            case "1":
                ////create user
                createUser();
                break;
            case "2":
                ///enter expense
                enterExpense();
                break;
            case "3":
                ///view all users
                viewAllUsers();
                break;
            case "4":
                ///view expense by user
                viewExpenseByUser();
                break;
            case "5":
                ///close application
                System.out.println("Application closed");
                break;
        }
    }

    private static void viewExpenseByUser() {
        clearConsole();
        System.out.print("Enter User Name: ");
        String username = scanner.next();

        /////convert collected data to a string that can be sent to the server
        HashMap<String, String> params = new HashMap<>();
        params.put("requestType", "getAllExpenseByUser");
        params.put("userName", username);
        String parameter = getPostDataString(params);
        MessageResponseTemplate response = sendRequestToServer(parameter);
//        System.out.println("Response Data: " + response.getResponse());
//        System.out.println("Data Type: " + response.getResponse().getClass().getName());
        ArrayList allExpenses = new ArrayList<>();
        try {
            allExpenses = (ArrayList) response.getResponse();
        } catch (Exception e) {
            System.out.println("Could not extract all expenses from response data. " + e.getMessage());
        }
        if (allExpenses != null && allExpenses.size() > 0) {
            System.out.println(" ");
            System.out.println(" ");
            System.out.println(" ");
            System.out.println("===Item Name======Item Amount=====Expense Date==========");
            for (int i = 0; i < allExpenses.size(); i++) {
                HashMap<String, String> currentExpense = (HashMap<String, String>) allExpenses.get(i);
                System.out.println("------------------------------------------");
                System.out.println("|   " + currentExpense.get("itemName") + "    |    " + currency + " " + currentExpense.get("itemAmount") + "  |  " + currentExpense.get("date"));
                System.out.println("------------------------------------------");
            }
            System.out.println("==============================================");
        } else {
            System.out.println("No expense for user: " + username);
        }
        showMenuExit();
    }

    private static void viewAllUsers() {
        clearConsole();
        System.out.println("Viewing all users");
        /////convert collected data to a string that can be sent to the server
        HashMap<String, String> params = new HashMap<>();
        params.put("requestType", "getAllUsers");
        String parameter = getPostDataString(params);
        MessageResponseTemplate response = sendRequestToServer(parameter);
        ArrayList allUsers = new ArrayList<>();
        try {
            allUsers = (ArrayList) response.getResponse();
        } catch (Exception e) {
            System.out.println("Could not extract users from response data. " + e.getMessage());

        }
        if (allUsers != null && allUsers.size() > 0) {
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("===S/N========User Name=====Full Name==========");
            for (int i = 0; i < allUsers.size(); i++) {
                HashMap<String, String> currentUser = (HashMap<String, String>) allUsers.get(i);
                System.out.println("------------------------------------------");
                System.out.println("|   " + currentUser.get("sn") + "    |    " + currentUser.get("userName") + "  |  " + currentUser.get("fullName"));
                System.out.println("------------------------------------------");
            }
            System.out.println("==============================================");
        } else {
            System.out.println("No user exist in storage");
        }
        showMenuExit();
    }

    private static void enterExpense() {
        clearConsole();
        System.out.print("Enter Username: ");
        String username = scanner.next();
        System.out.print("Enter Expense amount in (" + currency + "): ");
        String amount = scanner.next();
        System.out.print("Enter Item Name: ");
        String itemName = scanner.next();

        /////convert collected data to a string that can be sent to the server
        HashMap<String, String> params = new HashMap<>();
        params.put("requestType", "enterexpense");
        params.put("userName", username);
        params.put("amount", amount);
        params.put("itemName", itemName);
        String parameter = getPostDataString(params);
        MessageResponseTemplate response = sendRequestToServer(parameter);
        System.out.println(response.getMessage());

        showMenuExit();
    }

    private static void createUser() {
        clearConsole();
        System.out.print("Enter Username: ");
        String username = scanner.next();
        System.out.print("Enter First Name: ");
        String fullName = scanner.next();
        System.out.print("Enter Last Name:  ");
        fullName = fullName + " " + scanner.next();
//        System.out.println("User created");

        /////convert collected data to a string that can be sent to the server
        HashMap<String, String> params = new HashMap<>();
        params.put("requestType", "authenticate");
        params.put("userName", username);
        params.put("fullName", fullName);
        String parameter = getPostDataString(params);
        MessageResponseTemplate response = sendRequestToServer(parameter);
        System.out.println(response.getMessage());
        showMenuExit();
    }

    private static MessageResponseTemplate sendRequestToServer(String paramString) {
        MessageResponseTemplate response = new MessageResponseTemplate();
        try {
            StringBuilder responseData = new StringBuilder();
            URL url = new URL(serverURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("User-Agent", "Mozilla/5.0");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, StandardCharsets.UTF_8));
            writer.write(paramString);
            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    responseData.append("" + line);
                }
//                System.out.println(responseData);
                response = mapper.readValue(responseData.toString(), MessageResponseTemplate.class);
            }
        } catch (Exception e) {
            System.out.println("Error while communicating with server: " + e.getMessage());
            e.printStackTrace();
        }

        return response;
    }

    public static String getPostDataString(HashMap<String, String> params) {
        StringBuilder result = new StringBuilder();
        try {
            boolean first = true;
            for (Map.Entry<String, String> entry : params.entrySet()) {
                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
            }
        } catch (Exception e) {
            System.out.println("could not create parameter string because: " + e.getMessage());
            e.printStackTrace();
        }

        return result.toString();
    }


    private static void showMenuExit() {
        System.out.println("");
        System.out.println("");
        System.out.println("0. Back to Menu");
        System.out.println("1. Exit Application");
        String input = scanner.next();
        if (input.equalsIgnoreCase("0")) {
            displayMenu();
        } else if (input.equalsIgnoreCase("1")) {
            System.out.println("Application closed");
        } else {
            showMenuExit();
        }
    }


    public final static void clearConsole() {
        try {
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (IOException eio) {

        } catch (InterruptedException iex) {

        }
    }
}
